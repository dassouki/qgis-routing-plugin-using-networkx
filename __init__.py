def name():
    return "Routing"

def description():
    return "Routing Plugin"

def version():
    return "Version 0.1"

def icon():
    return "logo.png"

def qgisMinimumVersion():
    return "2.0"

def author():
    return "Ahmed Dassouki"

def email():
    return "dassouki@gmail.com"

def classFactory(iface):
    # load RoutingPlugin class from file RoutingPlugin
    from .routingplugin import RoutingPlugin
    return RoutingPlugin(iface)