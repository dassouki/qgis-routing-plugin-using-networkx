from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *

import os

from routingplugindialog import RoutingPluginDialog

# initialize Qt resources from file resouces.py
import resources


class RoutingPlugin:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # Create the dialog (after translation) and keep reference
        self.dlg = RoutingPluginDialog()

        # Capture coordinate related stuff:
        self.canvas = self.iface.mapCanvas()  # CHANGE

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/routing/logo.png"),
            u"Routing", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)
        self.action.setWhatsThis("Routing based on NetworkX library")

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&Routing", self.action)

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&Routing", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        # show the dialog

        self.dlg = RoutingPluginDialog()
        self.dlg.default_layout()
        # utils.setMapTool(self.iface,
        # RoutingPluginDialog.capture_pt_coordinates())
        if self.dlg.exec_():
            router = self.dlg.router
            router.route()
        # Run the dialog event loop
        # if result == 1:
            # Dialog()
