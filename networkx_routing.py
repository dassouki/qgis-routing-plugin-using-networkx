try:
    import numpy as np
except ImportError:
    raise ImportError("Routing requires numpy")

try:
    import networkx as nx
except ImportError:
    raise ImportError("Routing requires networkx")

try:
    from qgis.utils import iface
except ImportError:
    raise ImportError("Routing needs to run from within QGIS")

#Constants:
LYR_IN = str(iface.activeLayer().source())
LYR_OUT = '/Users/dassouki/Documents/projects/qgis-scripts/output'
PT_ORIGIN = (1,2)
PT_DEST = (3,4)
#  1: 'Shortest path', 2: 'A*', 3: 'Djikistra'
ROUTING_METHOD = 1

#helper functions:
def distance(pt_1, pt_2):
    """
    Euclidean distance between two points based on numpy arrays
    """
    pt_1 = np.array((pt_1[0], pt_1[1]))
    pt_2 = np.array((pt_2[0], pt_2[1]))
    return np.linalg.norm(pt_1-pt_2)


def closest_node(node, nodes):
    """
    find the closest point from a list of nodes to a given point that doesn't
    belong to the points
    """
    pt = []
    dist = 9999999
    for n in nodes:
        if distance(node, n) <= dist:
            dist = distance(node, n)
            pt = n
    return pt


def nearest_points(pt_origin, pt_dest, nodes):
    """
    returns a list of two nodes on the network layer that are the closest 
    Euclidean wise to the from and to points the user clicked on
    """
    pt_origin = closest_node(pt_origin, nodes)
    pt_dest = closest_node(pt_dest, nodes)
    return [pt_origin, pt_dest]


def load_lyr(lyr):
    return nx.read_shp(lyr)


def write_lyr(graph, path):
    return nx.write_shp(graph, path)


#Routing
def routing(pt_origin, pt_dest, method, lyr_in, lyr_out):
    
    lyr_in = load_lyr(lyr_in)
    nodes = lyr_in.nodes()
    pts = nearest_points(pt_origin, pt_dest, nodes)

    if method == 1:
        try:
            route = nx.shortest_path(lyr_in, pts[0], pts[1])
        except nx.NetworkXNoPath:
            print 'There was no path found'
    elif method == 2:
        try:
            route = nx.astar_path(lyr_in, pts[0], pts[1])
        except nx.NetworkXNoPath:
            print 'There was no path found'
    elif method == 3:
        try:
            route = nx.dijkstra_path(lyr_in, pts[0], pts[1])
        except nx.NetworkXNoPath:
            print 'There was no path found'

    route_graph = nx.subgraph(lyr_in, route)
    nx.write_shp(route_graph, lyr_out)


def main():
    routing(PT_ORIGIN, PT_DEST, ROUTING_METHOD, LYR_IN, LYR_OUT)


if __name__ == "__main__":
    main()
