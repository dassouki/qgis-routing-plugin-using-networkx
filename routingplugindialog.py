from PyQt4 import QtCore, QtGui
from qgis.core import *
from qgis.gui import *
from qgis import *

from qgis.utils import iface

from routing_dialog import Ui_Dialog
import utils


class Router():

    """
    returns the results of user input into the dialog
    """

    def __init__(self):
        lyr_selected = None
        lyr_index = None
        field_1selected = None
        field_2selected = None
        routing_algorithm = None
        from_pt = None
        to_pt = None
        path_to_save_csv = None

    def route(self):
        # use options to do stuff
        print self.lyr_selected, self.lyr_index, self.routing_algorithm


class RoutingPluginDialog(QtGui.QDialog):

    def __init__(self):
        QtGui.QDialog.__init__(self)

        # Set up the user interface from Designer.
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        # TODO Fix the attributes fields and the get layers function in
        # utils.py
        self.ui.combo_layer.currentIndexChanged[
            int].connect(self.populate_fields)

        self.ui.radio_custom_direction_values.toggled.connect(
            self.show_custom_direction)
        self.ui.radio_centroids.toggled.connect(self.show_centroid_fields)

        # handle button click behavior
        self.emitPoint = QgsMapToolEmitPoint(iface.mapCanvas())

        # user clicked on button
        self.ui.butt_from_pt.clicked.connect(self.capture_pt_coordinates_from)
        self.ui.butt_to_pt.clicked.connect(self.capture_pt_coordinates_to)

    def capture_pt_coordinates_from(self):
        # self.maptool = iface.mapCavnas().mapTool()
        self.hide()
        iface.mapCanvas().setMapTool(self.emitPoint)
        self.emitPoint.canvasClicked.connect(self.handleclick_from)

    def capture_pt_coordinates_to(self):
        # iface.mapCanvas().setMapTool(maptool)
        self.hide()
        iface.mapCanvas().setMapTool(self.emitPoint)
        self.emitPoint.canvasClicked.connect(self.handleclick_to)

    def handleclick_from(self, point, button):
        self.ui.txt_from_pt.setText(str(point))
        self.emitPoint.canvasClicked.disconnect(self.handleclick_from)
        # iface.mapCanvas().setMapTool(maptool)
        self.show()
        self.setVisible(True)

    def handleclick_to(self, point, button):
        self.ui.txt_to_pt.setText(str(point))
        self.emitPoint.canvasClicked.disconnect(self.handleclick_to)
        # iface.mapCanvas().setMapTool(maptool)
        self.show()
        self.setVisible(True)

    @property
    def router(self):
        """
        Returns the selected options as results that class Router() uses
        """
        ops = Router()
        ops.lyr_selected = self.ui.combo_layer.currentText()
        ops.lyr_index = self.ui.combo_layer.currentIndex()

        if self.ui.radio_algo_a_star.isChecked():
            ops.routing_algorithm = "A star"
        elif self.ui.radio_algo_djikstra.isChecked():
            ops.routing_algorithm = "Djikstra"
        elif self.ui.radio_algo_sp.isChecked():
            ops.routing_algorithm = "Shortest Path"
        else:
            # workaround since for some erason although a* is clicked when initialized,
            # it doesn't actually 'register' the click
            ops.routing_algorithm = "A star"

        return ops

    def default_layout(self):
        self.ui.progressBar.setValue(0)
        self.setWindowTitle(self.tr("Qgis Routing based on NetworkX"))
        if utils.layers_in_map_canvas():
            self.populate_layers()
        if utils.layers_in_map_canvas():
            self.populate_fields(0)
        self.ui.radio_algo_a_star.setChecked(True)
        self.ui.radio_default_direction_values.setChecked(True)

        self.ui.radio_pts.setChecked(True)
        # pts vs. centroid hide fields
        self.show_centroid_fields(False)
        # Directionality hide fields
        self.show_custom_direction(False)

    def show_custom_direction(self, enabled):
        if enabled:
            self.ui.label_7.setEnabled(True)
            self.ui.label_8.setEnabled(True)
            self.ui.label_9.setEnabled(True)
            self.ui.txt_both.setEnabled(True)
            self.ui.txt_direct.setEnabled(True)
            self.ui.txt_reverse.setEnabled(True)
        if not enabled:
            self.ui.label_7.setEnabled(False)
            self.ui.label_8.setEnabled(False)
            self.ui.label_9.setEnabled(False)
            self.ui.txt_both.setEnabled(False)
            self.ui.txt_direct.setEnabled(False)
            self.ui.txt_reverse.setEnabled(False)

    def show_centroid_fields(self, enabled):
        if enabled:
            self.ui.label_13.setEnabled(True)
            self.ui.combo_centroid.setEnabled(True)
            self.ui.chkBox_create_csv.setEnabled(True)
            self.ui.label_5.setEnabled(False)
            self.ui.label_6.setEnabled(False)
            self.ui.txt_from_pt.setEnabled(False)
            self.ui.txt_to_pt.setEnabled(False)
            self.ui.label_10.setEnabled(False)
            self.ui.label_11.setEnabled(False)
            self.ui.butt_from_pt.setEnabled(False)
            self.ui.butt_to_pt.setEnabled(False)
        if not enabled:
            self.ui.label_13.setEnabled(False)
            self.ui.combo_centroid.setEnabled(False)
            self.ui.chkBox_create_csv.setEnabled(False)
            self.ui.label_5.setEnabled(True)
            self.ui.label_6.setEnabled(True)
            self.ui.txt_from_pt.setEnabled(True)
            self.ui.txt_to_pt.setEnabled(True)
            self.ui.label_10.setEnabled(True)
            self.ui.label_11.setEnabled(True)
            self.ui.butt_from_pt.setEnabled(True)
            self.ui.butt_to_pt.setEnabled(True)

    def populate_layers(self):
        line_layers = utils.getLayerNames([QGis.Line])
        pt_layers = utils.getLayerNames([QGis.Point])
        # fill combo layer
        self.ui.combo_layer.blockSignals(True)
        self.ui.combo_layer.clear()
        for layer in line_layers:
            self.ui.combo_layer.addItem(layer.name(), layer)
        self.ui.combo_layer.blockSignals(False)

        # fill Centroid Layer
        self.ui.combo_centroid.blockSignals(True)
        self.ui.combo_centroid.clear()
        for layer in pt_layers:
            self.ui.combo_centroid.addItem(layer.name(), layer)
        self.ui.combo_centroid.blockSignals(False)

    def populate_fields(self, layer_index):
        """
            layer_index -> The selected index from the combobox
        """
        none_field = "--Optional--"
        # Clear fields and add NA item
        self.ui.combo_cost.clear()
        self.ui.combo_cost.addItem(unicode(none_field))
        self.ui.combo_directio_field.clear()
        self.ui.combo_directio_field.addItem(unicode(none_field))
        # get the user data for the selected item (when user changes value)
        layer = self.ui.combo_layer.itemData(layer_index)
        changed_field = utils.getFieldList(layer)

        for f in changed_field:
            self.ui.combo_cost.addItem(f.name())
            self.ui.combo_directio_field.addItem(f.name())
        # Direction field:
