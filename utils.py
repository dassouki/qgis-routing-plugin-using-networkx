# adapted from ftools_utils.py
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *

from qgis.utils import iface

import locale


# Return list of names of all layers in QgsMapLayerRegistry
def getLayerNames(vTypes):
    layermap = QgsMapLayerRegistry.instance().mapLayers()
    layerlist = []
    if vTypes == "all":
        for layer in layermap.iteritems():
            layerlist.append(layer)
    else:
        for name, layer in layermap.iteritems():
            if layer.type() == QgsMapLayer.VectorLayer:
                if layer.geometryType() in vTypes:
                    layerlist.append(layer)
            elif layer.type() == QgsMapLayer.RasterLayer:
                if "Raster" in vTypes:
                    layerlist.append(layer)
    # return sorted(layerlist, cmp=locale.strcoll)
    return layerlist

# For use with memory provider/layer, converts QGis vector type definition
# to simple string


def getVectorTypeAsString(vlayer):
    if vlayer.geometryType() == QGis.Polygon:
        return "Polygon"
    elif vlayer.geometryType() == QGis.Line:
        return "LineString"
    elif vlayer.geometryType() == QGis.Point:
        return "Point"
    else:
        return False

# Return if there are any layers in the map canvas


def layers_in_map_canvas():
    return (QgsMapLayerRegistry.instance().mapLayers())


# Return the field list of a vector layer
def getFieldList(vlayer):
    return vlayer.dataProvider().fields()


# Return list of names of all fields from input QgsVectorLayer
def getFieldNames(layer_index):
    sorted([field.name() for field in getFieldList(layer_index)])


# point emit tool
def setMapTool(iface, callback):
    emitPoint = QgsMapToolEmitPoint(iface.mapCanvas())
    iface.mapCanvas().setMapTool(emitPoint)
    emitPoint.canvasClicked.connect(callback)


def capture_pt_coordinates(vlayer):
    provider = vlayer.dataProvider()
    feat = QgsFeature()
    while(provider.nextFeature(feat)):
        geom = feat.geometry()
        x = geom.asPoint().x()
        y = geom.asPoint().y()
        print x, y

    return x, y
