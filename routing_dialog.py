# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'routing_dialog.ui'
#
# Created: Sun Jul  7 11:24:15 2013
#      by: PyQt4 UI code generator 4.9.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(719, 470)
        self.gridLayout = QtGui.QGridLayout(Dialog)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.gridLayout.addWidget(self.buttonBox, 2, 1, 1, 1)
        self.progressBar = QtGui.QProgressBar(Dialog)
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.gridLayout.addWidget(self.progressBar, 2, 0, 1, 1)
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.label_3 = QtGui.QLabel(Dialog)
        self.label_3.setMinimumSize(QtCore.QSize(0, 50))
        self.label_3.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_3.setWordWrap(True)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout_2.addWidget(self.label_3, 0, 0, 1, 6)
        self.groupBox_pts = QtGui.QGroupBox(Dialog)
        self.groupBox_pts.setObjectName(_fromUtf8("groupBox_pts"))
        self.gridLayout_6 = QtGui.QGridLayout(self.groupBox_pts)
        self.gridLayout_6.setObjectName(_fromUtf8("gridLayout_6"))
        self.label_5 = QtGui.QLabel(self.groupBox_pts)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout_6.addWidget(self.label_5, 1, 0, 1, 1)
        self.txt_from_pt = QtGui.QLineEdit(self.groupBox_pts)
        self.txt_from_pt.setObjectName(_fromUtf8("txt_from_pt"))
        self.gridLayout_6.addWidget(self.txt_from_pt, 1, 1, 1, 1)
        self.label_10 = QtGui.QLabel(self.groupBox_pts)
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.gridLayout_6.addWidget(self.label_10, 1, 2, 1, 1)
        self.butt_from_pt = QtGui.QPushButton(self.groupBox_pts)
        self.butt_from_pt.setObjectName(_fromUtf8("butt_from_pt"))
        self.gridLayout_6.addWidget(self.butt_from_pt, 1, 3, 1, 1)
        self.label_6 = QtGui.QLabel(self.groupBox_pts)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.gridLayout_6.addWidget(self.label_6, 2, 0, 1, 1)
        self.txt_to_pt = QtGui.QLineEdit(self.groupBox_pts)
        self.txt_to_pt.setObjectName(_fromUtf8("txt_to_pt"))
        self.gridLayout_6.addWidget(self.txt_to_pt, 2, 1, 1, 1)
        self.label_11 = QtGui.QLabel(self.groupBox_pts)
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.gridLayout_6.addWidget(self.label_11, 2, 2, 1, 1)
        self.butt_to_pt = QtGui.QPushButton(self.groupBox_pts)
        self.butt_to_pt.setObjectName(_fromUtf8("butt_to_pt"))
        self.gridLayout_6.addWidget(self.butt_to_pt, 2, 3, 1, 1)
        self.radio_pts = QtGui.QRadioButton(self.groupBox_pts)
        self.radio_pts.setObjectName(_fromUtf8("radio_pts"))
        self.gridLayout_6.addWidget(self.radio_pts, 0, 0, 1, 4)
        self.radio_centroids = QtGui.QRadioButton(self.groupBox_pts)
        self.radio_centroids.setObjectName(_fromUtf8("radio_centroids"))
        self.gridLayout_6.addWidget(self.radio_centroids, 3, 0, 1, 4)
        self.combo_centroid = QtGui.QComboBox(self.groupBox_pts)
        self.combo_centroid.setObjectName(_fromUtf8("combo_centroid"))
        self.gridLayout_6.addWidget(self.combo_centroid, 4, 1, 1, 3)
        self.label_13 = QtGui.QLabel(self.groupBox_pts)
        self.label_13.setObjectName(_fromUtf8("label_13"))
        self.gridLayout_6.addWidget(self.label_13, 4, 0, 1, 1)
        self.chkBox_create_csv = QtGui.QCheckBox(self.groupBox_pts)
        self.chkBox_create_csv.setObjectName(_fromUtf8("chkBox_create_csv"))
        self.gridLayout_6.addWidget(self.chkBox_create_csv, 5, 1, 1, 3)
        self.gridLayout_2.addWidget(self.groupBox_pts, 1, 3, 2, 3)
        self.groupBox_data = QtGui.QGroupBox(Dialog)
        self.groupBox_data.setObjectName(_fromUtf8("groupBox_data"))
        self.gridLayout_5 = QtGui.QGridLayout(self.groupBox_data)
        self.gridLayout_5.setObjectName(_fromUtf8("gridLayout_5"))
        self.label = QtGui.QLabel(self.groupBox_data)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_5.addWidget(self.label, 0, 0, 1, 1)
        self.combo_layer = QtGui.QComboBox(self.groupBox_data)
        self.combo_layer.setObjectName(_fromUtf8("combo_layer"))
        self.gridLayout_5.addWidget(self.combo_layer, 0, 1, 1, 2)
        self.label_2 = QtGui.QLabel(self.groupBox_data)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout_5.addWidget(self.label_2, 1, 0, 1, 1)
        self.combo_directio_field = QtGui.QComboBox(self.groupBox_data)
        self.combo_directio_field.setObjectName(_fromUtf8("combo_directio_field"))
        self.gridLayout_5.addWidget(self.combo_directio_field, 1, 1, 1, 2)
        self.label_12 = QtGui.QLabel(self.groupBox_data)
        self.label_12.setObjectName(_fromUtf8("label_12"))
        self.gridLayout_5.addWidget(self.label_12, 2, 0, 1, 1)
        self.combo_cost = QtGui.QComboBox(self.groupBox_data)
        self.combo_cost.setObjectName(_fromUtf8("combo_cost"))
        self.gridLayout_5.addWidget(self.combo_cost, 2, 1, 1, 2)
        self.gridLayout_2.addWidget(self.groupBox_data, 1, 0, 1, 3)
        self.groupBox_algo = QtGui.QGroupBox(Dialog)
        self.groupBox_algo.setObjectName(_fromUtf8("groupBox_algo"))
        self.verticalLayout = QtGui.QVBoxLayout(self.groupBox_algo)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.radio_algo_a_star = QtGui.QRadioButton(self.groupBox_algo)
        self.radio_algo_a_star.setObjectName(_fromUtf8("radio_algo_a_star"))
        self.verticalLayout.addWidget(self.radio_algo_a_star)
        self.radio_algo_djikstra = QtGui.QRadioButton(self.groupBox_algo)
        self.radio_algo_djikstra.setObjectName(_fromUtf8("radio_algo_djikstra"))
        self.verticalLayout.addWidget(self.radio_algo_djikstra)
        self.radio_algo_sp = QtGui.QRadioButton(self.groupBox_algo)
        self.radio_algo_sp.setObjectName(_fromUtf8("radio_algo_sp"))
        self.verticalLayout.addWidget(self.radio_algo_sp)
        self.gridLayout_2.addWidget(self.groupBox_algo, 2, 0, 1, 1)
        self.groupBox_options = QtGui.QGroupBox(Dialog)
        self.groupBox_options.setObjectName(_fromUtf8("groupBox_options"))
        self.gridLayout_3 = QtGui.QGridLayout(self.groupBox_options)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.radio_default_direction_values = QtGui.QRadioButton(self.groupBox_options)
        self.radio_default_direction_values.setObjectName(_fromUtf8("radio_default_direction_values"))
        self.gridLayout_3.addWidget(self.radio_default_direction_values, 1, 0, 1, 2)
        self.radio_custom_direction_values = QtGui.QRadioButton(self.groupBox_options)
        self.radio_custom_direction_values.setObjectName(_fromUtf8("radio_custom_direction_values"))
        self.gridLayout_3.addWidget(self.radio_custom_direction_values, 4, 0, 1, 2)
        self.gridLayout_4 = QtGui.QGridLayout()
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.label_9 = QtGui.QLabel(self.groupBox_options)
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.gridLayout_4.addWidget(self.label_9, 0, 0, 1, 1)
        self.label_8 = QtGui.QLabel(self.groupBox_options)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.gridLayout_4.addWidget(self.label_8, 1, 0, 1, 1)
        self.label_7 = QtGui.QLabel(self.groupBox_options)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.gridLayout_4.addWidget(self.label_7, 2, 0, 2, 1)
        self.txt_both = QtGui.QLineEdit(self.groupBox_options)
        self.txt_both.setObjectName(_fromUtf8("txt_both"))
        self.gridLayout_4.addWidget(self.txt_both, 0, 1, 1, 1)
        self.txt_reverse = QtGui.QLineEdit(self.groupBox_options)
        self.txt_reverse.setObjectName(_fromUtf8("txt_reverse"))
        self.gridLayout_4.addWidget(self.txt_reverse, 1, 1, 1, 1)
        self.txt_direct = QtGui.QLineEdit(self.groupBox_options)
        self.txt_direct.setObjectName(_fromUtf8("txt_direct"))
        self.gridLayout_4.addWidget(self.txt_direct, 2, 1, 2, 1)
        self.gridLayout_3.addLayout(self.gridLayout_4, 5, 0, 1, 2)
        self.gridLayout_2.addWidget(self.groupBox_options, 2, 1, 1, 2)
        self.chkBox_load_when_completed = QtGui.QCheckBox(Dialog)
        self.chkBox_load_when_completed.setObjectName(_fromUtf8("chkBox_load_when_completed"))
        self.gridLayout_2.addWidget(self.chkBox_load_when_completed, 3, 0, 1, 2)
        self.label_4 = QtGui.QLabel(Dialog)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout_2.addWidget(self.label_4, 3, 2, 1, 1)
        self.txt_file_path = QtGui.QLineEdit(Dialog)
        self.txt_file_path.setObjectName(_fromUtf8("txt_file_path"))
        self.gridLayout_2.addWidget(self.txt_file_path, 3, 3, 1, 1)
        self.butt_browse = QtGui.QPushButton(Dialog)
        self.butt_browse.setObjectName(_fromUtf8("butt_browse"))
        self.gridLayout_2.addWidget(self.butt_browse, 3, 4, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 2)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Dialog", "QGIS routing plugin uses QGIS\'s networkx plugin to build a route for your specified points", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_pts.setTitle(QtGui.QApplication.translate("Dialog", "Routing", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("Dialog", "From Point", None, QtGui.QApplication.UnicodeUTF8))
        self.label_10.setText(QtGui.QApplication.translate("Dialog", "or", None, QtGui.QApplication.UnicodeUTF8))
        self.butt_from_pt.setText(QtGui.QApplication.translate("Dialog", "Capture", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("Dialog", "To Point", None, QtGui.QApplication.UnicodeUTF8))
        self.label_11.setText(QtGui.QApplication.translate("Dialog", "or", None, QtGui.QApplication.UnicodeUTF8))
        self.butt_to_pt.setText(QtGui.QApplication.translate("Dialog", "Capture", None, QtGui.QApplication.UnicodeUTF8))
        self.radio_pts.setText(QtGui.QApplication.translate("Dialog", "Use two points", None, QtGui.QApplication.UnicodeUTF8))
        self.radio_centroids.setText(QtGui.QApplication.translate("Dialog", "Route between all points of a point layer", None, QtGui.QApplication.UnicodeUTF8))
        self.label_13.setText(QtGui.QApplication.translate("Dialog", "Points  Layer", None, QtGui.QApplication.UnicodeUTF8))
        self.chkBox_create_csv.setText(QtGui.QApplication.translate("Dialog", "Make CSV of results", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_data.setTitle(QtGui.QApplication.translate("Dialog", "Layer Options", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Dialog", "Road Layer", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Dialog", "Direction Field (Optional)", None, QtGui.QApplication.UnicodeUTF8))
        self.label_12.setText(QtGui.QApplication.translate("Dialog", "Cost Field (Optional)", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_algo.setTitle(QtGui.QApplication.translate("Dialog", "Routing Algorithm", None, QtGui.QApplication.UnicodeUTF8))
        self.radio_algo_a_star.setText(QtGui.QApplication.translate("Dialog", "A star", None, QtGui.QApplication.UnicodeUTF8))
        self.radio_algo_djikstra.setText(QtGui.QApplication.translate("Dialog", "Djikstra", None, QtGui.QApplication.UnicodeUTF8))
        self.radio_algo_sp.setText(QtGui.QApplication.translate("Dialog", "Shortest Path", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_options.setTitle(QtGui.QApplication.translate("Dialog", "Direction Options", None, QtGui.QApplication.UnicodeUTF8))
        self.radio_default_direction_values.setText(QtGui.QApplication.translate("Dialog", "Default Direction Values", None, QtGui.QApplication.UnicodeUTF8))
        self.radio_custom_direction_values.setText(QtGui.QApplication.translate("Dialog", "Custom Direction Values", None, QtGui.QApplication.UnicodeUTF8))
        self.label_9.setText(QtGui.QApplication.translate("Dialog", "Both", None, QtGui.QApplication.UnicodeUTF8))
        self.label_8.setText(QtGui.QApplication.translate("Dialog", "Reverse", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(QtGui.QApplication.translate("Dialog", "Direct", None, QtGui.QApplication.UnicodeUTF8))
        self.chkBox_load_when_completed.setText(QtGui.QApplication.translate("Dialog", "Load in QGIS when completed?", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("Dialog", "Output File", None, QtGui.QApplication.UnicodeUTF8))
        self.butt_browse.setText(QtGui.QApplication.translate("Dialog", "Browse", None, QtGui.QApplication.UnicodeUTF8))

